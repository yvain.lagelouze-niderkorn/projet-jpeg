# From RGB format to YCbCr format

import matplotlib.pyplot as plt
from PIL import Image
import numpy as np

# image = Image.open("Rose.jpg")
# plt.imshow(image)
# plt.show()


# Converting from RGB to YCbCr

def lumchro(R, V, B):  # recommandation 601
    Lum = 0.299 * R + 0.587 * V + 0.144 * B
    Cb = B - Lum
    Cr = R - Lum
    return(Lum, Cb, Cr)


def lumchro_reverse(Y, Cb, Cr):
    R = Y + Cr
    B = Y + Cb
    V = 0.9488926746 * Y - 0.2453151618 * Cb - 0.5093696763 * Cr
    return([int(R), int(V), int(B)])

# Conversion of an image-matrix RGB to matrix Y, Cb et Cr
#
# Input : matrix RGB
# Output : 3 matrix Y, Cb et Cr


def lumchro_tabl(tabl):
    pattern = [[0 for j in range(len(tabl[0]))] for i in range(len(tabl))]
    luminance = (np.array(pattern.copy())).astype('float64')
    chroCb = (np.array(pattern.copy())).astype('float64')
    chroCr = (np.array(pattern.copy())).astype('float64')
    for i in range(len(tabl)):
        for j in range(len(tabl[0])):
            R, V, B = tabl[i][j][0], tabl[i][j][1], tabl[i][j][2]
            Y, Cb, Cr = lumchro(R, V, B)
            luminance[i][j] = Y
            chroCb[i][j] = Cb
            chroCr[i][j] = Cr
    return(luminance, chroCb, chroCr)


# Reverse fonction from three matrix Y, Cb et Cr to RGB

def versRVB(luminance, chroCb, chroCr):
    mat = [[(lumchro_reverse(luminance[i][j], chroCb[i][j], chroCr[i][j]))
            for j in range(len(luminance[0]))] for i in range(len(luminance))]
    return(mat)


"""
R, V, B = 156, 210, 32
Y, Cb, Cr = lumchro(R, V, B)

print(R, V, B, Y, Cb, Cr)
print(lumchro_reverse(Y, Cb, Cr))
"""


"""
luminance = [[139, 144, 149, 153, 155, 155, 155, 155], [144, 151, 153, 156, 159, 156, 156, 156], [150, 155, 160, 163, 158, 156, 156, 156], [159, 161, 162, 160, 160, 159, 159, 159], [
    159, 160, 161, 162, 162, 155, 155, 155], [161, 161, 161, 161, 160, 157, 157, 157], [162, 162, 161, 163, 162, 157, 157, 157], [162, 162, 161, 161, 163, 158, 158, 158]]

chroCb = [[139, 144, 149, 153, 155, 155, 155, 155], [144, 151, 153, 156, 159, 156, 156, 156], [150, 155, 160, 163, 158, 156, 156, 156], [159, 161, 162, 160, 160, 159, 159, 159], [
    159, 160, 161, 162, 162, 155, 155, 155], [161, 161, 161, 161, 160, 157, 157, 157], [162, 162, 161, 163, 162, 157, 157, 157], [162, 162, 161, 161, 163, 158, 158, 158]]

chroCr = [[139, 144, 149, 153, 155, 155, 155, 155], [144, 151, 153, 156, 159, 156, 156, 156], [150, 155, 160, 163, 158, 156, 156, 156], [159, 161, 162, 160, 160, 159, 159, 159], [
    159, 160, 161, 162, 162, 155, 155, 155], [161, 161, 161, 161, 160, 157, 157, 157], [162, 162, 161, 163, 162, 157, 157, 157], [162, 162, 161, 161, 163, 158, 158, 158]]

vers = versRVB(luminance, chroCb, chroCr)
print(vers)
"""
"""
image_init = Image.open("image.jpg")
image = (np.asarray(image_init)).astype('float64')


print(image + image)
print(lumchro_tabl(image)[0])
"""
