
# Conversion from RGB's format to the format of Luminance / Chrominance (YCbCr)

import matplotlib.pyplot as plt
from PIL import Image
import numpy as np

# Converting from RGB to YCbCr


def lumchro(R, V, B):  # recommandation 601
    Lum = 0.299 * R + 0.587 * V + 0.144 * B
    Cb = B - Lum
    Cr = R - Lum
    return(Lum, Cb, Cr)


def lumchro_reverse(Y, Cb, Cr):
    R = Y + Cr
    B = Y + Cb
    V = 0.9488926746 * Y - 0.2453151618 * Cb - 0.5093696763 * Cr
    return([int(R), int(V), int(B)])

# Conversion of an image-matrix RGB to matrix Y, Cb et Cr
#
# Input : matrix RGB
# Output : 3 matrix Y, Cb et Cr


def lumchro_tabl(tabl):
    pattern = [[0 for j in range(len(tabl[0]))] for i in range(len(tabl))]
    luminance = (np.array(pattern.copy())).astype('float64')
    chroCb = (np.array(pattern.copy())).astype('float64')
    chroCr = (np.array(pattern.copy())).astype('float64')
    for i in range(len(tabl)):
        for j in range(len(tabl[0])):
            R, V, B = tabl[i][j][0], tabl[i][j][1], tabl[i][j][2]
            Y, Cb, Cr = lumchro(R, V, B)
            luminance[i][j] = Y
            chroCb[i][j] = Cb
            chroCr[i][j] = Cr
    return(luminance, chroCb, chroCr)


# Reverse fonction from three matrix Y, Cb et Cr to RGB

def versRVB(luminance, chroCb, chroCr):
    mat = [[(lumchro_reverse(luminance[i][j], chroCb[i][j], chroCr[i][j]))
            for j in range(len(luminance[0]))] for i in range(len(luminance))]
    return(mat)
