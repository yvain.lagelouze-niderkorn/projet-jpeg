# File to calculate error

from math import sqrt
import numpy as np


def norm(M):
    S = 0
    for i in range(len(M)):
        for j in range(len(M[0])):
            S += M[i][j]**2
    return(sqrt(S))


def error(M, N):
    Mr = [[M[i][j][0] for j in range(len(M[0]))] for i in range(len(M))]
    Mg = [[M[i][j][1] for j in range(len(M[0]))] for i in range(len(M))]
    Mb = [[M[i][j][2] for j in range(len(M[0]))] for i in range(len(M))]
    Nr = [[N[i][j][0] for j in range(len(N[0]))] for i in range(len(N))]
    Ng = [[N[i][j][1] for j in range(len(N[0]))] for i in range(len(N))]
    Nb = [[N[i][j][2] for j in range(len(N[0]))] for i in range(len(N))]
    eps = norm(np.array(Mr)-np.array(Nr))/norm(Mr) + \
        norm(np.array(Mg)-np.array(Ng))/norm(Mg) + \
        norm(np.array(Mb)-np.array(Nb))/norm(Mb)
    return(eps/3)
