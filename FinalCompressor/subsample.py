
# Subsample of chrominance in squares of 2x2
#
# Input : matrix of chrominance (Cr or Cb as you wish)
# Output : new chrominance subsampled matrix

def subsample420(tabl):  # subsample 4:2:0
    leng = len(tabl[0])
    widt = len(tabl)
    ntabl = tabl.copy()  # new image with chrominance subsampled
    for i in range(widt//2):
        for j in range(leng//2):
            val = tabl[2*i][2*j]
            ntabl[2*i+1][2*j], ntabl[2*i][2 * j+1], ntabl[2*i +
                                                          1][2*j+1] = val, val, val  # the 3 other pixels updated
    return(ntabl)


def subsample_new(tabl):
    leng = len(tabl[0])
    widt = len(tabl)
    # new image with chrominance subsampled
    ntabl = [[0 for j in range(leng//2)] for i in range(widt//2)]
    for i in range(widt//2):
        for j in range(leng//2):
            val = tabl[2*i][2*j]
            ntabl[i][j] = val
    return(ntabl)


def subsample_reverse(tabl):
    leng = len(tabl)
    # new image with chrominance subsampled
    ntabl = [[tabl[i//2][j//2] for j in range(2*leng)] for i in range(2*leng)]
    return(ntabl)
