# Zigzag function right before RLE encoding
#
# Input : frequency matrix 8x8
# Output : list in zigzag order

from math import sqrt


def zigzag(Mat):
    leng = len(Mat)
    if True:
        L = [0 for k in range(leng*leng)]  # initialisation
        cmpt = 0
        for diag in range(leng):  # top left side, the 8 first diagonals
            if diag % 2 == 0:  # if the diagonal is even, from bottom left to top right
                for k in range(diag + 1):
                    L[cmpt] = Mat[diag-k][k]
                    cmpt += 1
            else:  # if diagonal is odd, from top right to bottom left
                for k in range(diag + 1):
                    L[cmpt] = Mat[k][diag-k]
                    cmpt += 1
        for diag in range(leng-1):  # bottom right side, the last 7 diagonals
            if diag % 2 == 0:
                for k in range(leng-1-diag):
                    L[cmpt] = Mat[leng-1-k][diag + k+1]
                    cmpt += 1
            else:
                for k in range(leng-1 - diag):
                    L[cmpt] = Mat[diag + k+1][leng-1 - k]
                    cmpt += 1
    return(L)


# Function zigzag_reverse
#
# Input : a list with zigzag order
# Output : corresponding frequency matrix


def zigzag_reverse(L):
    leng = int(sqrt(len(L)))
    if True:
        Mat = [[0 for j in range(leng)] for i in range(leng)]
        cmpt = 0
        for diag in range(leng):  # top left side, the 8 first diagonals
            if diag % 2 == 0:  # if the diagonal is even, from bottom left to top right
                for k in range(diag + 1):
                    Mat[diag-k][k] = L[cmpt]
                    cmpt += 1
            else:  # if diagonal is odd, from top right to bottom left
                for k in range(diag + 1):
                    Mat[k][diag-k] = L[cmpt]
                    cmpt += 1
        for diag in range(leng-1):  # bottom right side, the last 7 diagonals
            if diag % 2 == 0:
                for k in range(leng-1-diag):
                    Mat[leng-1-k][diag + k+1] = L[cmpt]
                    cmpt += 1
            else:
                for k in range(leng-1 - diag):
                    Mat[diag + k+1][leng-1 - k] = L[cmpt]
                    cmpt += 1
        return(Mat)
