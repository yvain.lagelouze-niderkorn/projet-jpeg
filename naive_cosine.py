# Algorithme naïf de transformation discrète consinusoïdale (DCT en anglais)

from math import *

# Coefficient caractéristique de la DCT


def C(x):
    if x == 0:
        return(1/sqrt(2))
    else:
        return(1)


# DCT d'une coordonnée (i,j) de la matrice spatiale

def cosine(tableau, i, j):
    somme = 0
    N = len(tableau)
    M = len(tableau[0])
    for x in range(N):
        for y in range(M):
            somme += tableau[x][y] * cos((2*x + 1)*i *
                                         pi / (2*N)) * cos((2*y + 1)*j*pi / (2*M))
    somme *= 2 * C(i) * C(j) / N
    return(somme)

# DCT
#
# Entrée : matrice spatiale
# Sortie : matrice de fréquences par DCT


def cosine_transform(tableau):
    N = len(tableau)
    M = len(tableau[0])
    transformed = [[0 for k in range(M)] for i in range(N)]
    for i in range(N):
        for j in range(M):
            transformed[i][j] = (cosine(tableau, i, j))
    return(transformed)


# DCT inverse d'une coordonnée (x,y) de la matrice de fréquence
def reverse_cosine(tableau, x, y):
    somme = 0
    N = len(tableau)
    M = len(tableau[0])
    for i in range(N):
        for j in range(M):
            somme += C(i) * C(j) * tableau[i][j] * cos((2*x + 1)*i *
                                                       pi / (2*N)) * cos((2*y + 1)*j*pi / (2*M))
    somme *= 2 / N
    return(somme)

# DCT inverse
#
# Entrée : matrice de fréquence
# Sortie : matrice spatiale par DCT inverse


def reverse_cosine_transform(tableau):  # transformée en cosinus inverse
    N = len(tableau)
    M = len(tableau[0])
    # matrice spatialle originelle
    reverse = [[0 for k in range(M)] for i in range(N)]
    for x in range(N):
        for y in range(M):
            reverse[x][y] = round(reverse_cosine(tableau, x, y))
    return(reverse)


# Quantification de F par Q
#
# Entrée : une matrice de fréquence (obtenue par DCT) et un masque Q
# Sortie : nouvelle matrice quantifiée, les fréquences hautes sont "fondues" par le masque

def quantif(F, Q):
    new = F.copy()
    for i in range(len(new)):
        for j in range(len(new[0])):
            new[i][j] = round(F[i][j]/Q[i][j])
    return(new)


tableau = [[139, 144, 149, 153, 155, 155, 155, 155], [144, 151, 153, 156, 159, 156, 156, 156], [150, 155, 160, 163, 158, 156, 156, 156], [159, 161, 162, 160, 160, 159, 159, 159], [
    159, 160, 161, 162, 162, 155, 155, 155], [161, 161, 161, 161, 160, 157, 157, 157], [162, 162, 161, 163, 162, 157, 157, 157], [162, 162, 161, 161, 163, 158, 158, 158]]
"""
print(len(tableau), len(tableau[0]))


masque = [[16, 11, 10, 16, 24, 40, 51, 61], [12, 12, 14, 19, 26, 58, 60, 55], [14, 13, 16, 24, 40, 57, 69, 56], [14, 17, 22, 29, 51, 87, 80, 62], [
    18, 22, 37, 56, 68, 109, 103, 77], [24, 35, 55, 64, 81, 104, 113, 92], [49, 64, 78, 87, 103, 121, 120, 101], [72, 92, 95, 98, 112, 100, 103, 99]]


costrans = cosine_transform(tableau)
print(costrans)

test = [[16, 6, 0, 2, 0, -2, 0, -2], [8, 4, -2, -2, 0, 2, 2, 0], [0, 2, 4, 0, 0, 0, -2, 0], [4, -2, 0, 0, 0, 0, 0,
                                                                                             0], [0, 0, 0, 0, 0, 0, 0, 0], [-2, 2, 0, 0, 0, 0, 0, 0], [0, 0, -2, 0, 0, 0, 0, 0], [-2, 0, 0, 0, 0, 0, 0, 0]]

print(cosine(tableau, 1, 0))

print(quantif(cosine_transform(tableau), masque))


print(reverse_cosine_transform(test))
"""
