# This part is useful for RLE encoding and decoding


# This function "occurrences" allows to find the number of occurrences of a char in a string


def occurrences(string, i):
    ''' Returns the number of consecutive occurrences of the i-th character in string
    and the index of the last occurrence 
    '''
    n = 0
    ref = string[i]
    check = True
    for char in string[i:]:
        if not check:
            return {'letter': ref, 'occur': n, 'next_index': n+i}
        else:
            if char == ref:
                n += 1
            else:
                check = False
    return {'letter': ref, 'occur': n, 'next_index': n+i}


input = '1111111111111111'


def rle(string):
    ''' Encode in the RLE format a given string
    The chosen char is * for giving the number of occurences
    and , for separating each number
    '''
    output = ''
    length = len(string)
    index = 0
    while index < length:
        # if it is a zero, we note between * the number of occurrences
        if str(string[index]) == '0':
            occur = occurrences(string, index)
            index = occur['next_index']
            oc = occur['occur']
            let = occur['letter']
            output += '*' + str(oc)+'*'+str(let) + ","  # and we end with ,
        else:
            # for other char than 0, we don't mention the occurrences
            output += str(string[index]) + ","
            index += 1
    return (output[:len(output)-1])


def str_to_int(string):
    '''Converts a RLE string (of numbers separated by *)
    into an int list
    '''
    if string[0] == "*":
        index = 1
        while string[index] != "*":  # The symbol * is at the position "index"
            index += 1
        nb = int(string[1:index])
        intLst = [0 for j in range(nb)]
    else:
        intLst = [int(string)]
    return(intLst)


# Input : string encoding in RLE
# Output : list of integer
def decode_rle(string):
    '''Decodes an RLE-encoded string
    The RLE separator must be *
    '''
    result = []
    index = 0
    for k in range(len(string)):
        char = string[k]  # we find each sequence separated by ","
        if char == ",":
            result += (str_to_int(string[index:k]))
            index = k+1  # New beginning for the next number
    result += (str_to_int(string[index:len(string)]))
    return result


"""
encoded_msg = rle(input)
print(encoded_msg)

decoded_msg = decode_rle(encoded_msg)
print(decoded_msg)
print(len(decoded_msg))
"""

# print(occurrences("143333394206428350", 2))
# print(rle("12331014014000000400040420000"))

"""
print(rle(["1", "10", "10502", "0", "0", "0", "10", "0"]))

print(str_to_int("*3*0"))

print(decode_rle("1,10,10502,*3*0,10,*1*0"))
"""

# print(decode_rle("-12,*1*0,-1,*61*0"))
