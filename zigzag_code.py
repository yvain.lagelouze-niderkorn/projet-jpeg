# Fonction zigzag avant le codage RLE
#
# Entrée : matrice de fréquences 8x8
# Sortie : liste donnant l'ordre en zigzag


def zigzag(Mat):
    if len(Mat) != 8 or len(Mat[0]) != 8:
        print("Erreur, taille incohérente")
    else:
        L = [0 for k in range(64)]  # initialisation
        cmpt = 0
        for diag in range(8):  # partie haut gauche, les 8 premières diagonales
            if diag % 2 == 0:  # si diagonale paire, sens de bas gauche à haut droit
                for k in range(diag + 1):
                    L[cmpt] = Mat[diag-k][k]
                    cmpt += 1
            else:  # si diagonale impaire, sens de haut droit à bas gauche
                for k in range(diag + 1):
                    L[cmpt] = Mat[k][diag-k]
                    cmpt += 1
        for diag in range(7):  # partie bas droite, les 7 dernières diagonales
            if diag % 2 == 0:
                for k in range(7-diag):
                    L[cmpt] = Mat[7-k][diag + k+1]
                    cmpt += 1
            else:
                for k in range(7 - diag):
                    L[cmpt] = Mat[diag + k+1][7 - k]
                    cmpt += 1
    return(L)


"""def lst_to_str(L):  # de la liste des amplitudes à un string, séparées par des virgules
    string = ""
    for char in L:
        string += str(char)
        string += ","
    return(string[:len(string)-1])  # on retire la dernière virgule
"""

# Fonction zigzag inverse
#
# Entrée : une liste avec l'ordre en zigzag
# Sortie : la matrice de fréquences correspondante


def zigzag_reverse(L):
    if len(L) != 64:
        print("Erreur, longueur incohérente")
    else:
        Mat = [[0 for j in range(8)] for i in range(8)]
        cmpt = 0
        for diag in range(8):  # partie haut gauche, les 8 premières diagonales
            if diag % 2 == 0:  # si diagonale paire, sens de bas gauche à haut droit
                for k in range(diag + 1):
                    Mat[diag-k][k] = L[cmpt]
                    cmpt += 1
            else:  # si diagonale impaire, sens de haut droit à bas gauche
                for k in range(diag + 1):
                    Mat[k][diag-k] = L[cmpt]
                    cmpt += 1
        for diag in range(7):  # partie bas droite, les 7 dernières diagonales
            if diag % 2 == 0:
                for k in range(7-diag):
                    Mat[7-k][diag + k+1] = L[cmpt]
                    cmpt += 1
            else:
                for k in range(7 - diag):
                    Mat[diag + k+1][7 - k] = L[cmpt]
                    cmpt += 1
        return(Mat)


"""
M = [[j + 8*i for j in range(8)] for i in range(8)]

print(M)

L = zigzag(M)
print(str(L))

print(zigzag_reverse(L))
"""
