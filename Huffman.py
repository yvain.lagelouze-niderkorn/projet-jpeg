# We pass from RLE encoding to Huffman encoding in order to again optimise storage

import dahuffman as huffman

# Construction of a frequency table
#
# Input : text
# Output: a dictionary that associates every char to its frequency in the text


def table_frequencies(texte):
    table = {}
    for character in texte:
        if character in table:
            table[character] = table[character] + 1
        else:
            table[character] = 1
    return table

# Takes a RLE sequence and converts it into a Huffman sequence


def fromRLE_to_Huffman(rle_seq):
    table = table_frequencies(rle_seq)
    codec = huffman.HuffmanCodec.from_frequencies(table)
    encoded = codec.encode(rle_seq)
    return(encoded, table)


# Takes a Huffman sequence and frequency table and gives RLE sequence
def decode_huffman(encoded, table):
    codec = huffman.HuffmanCodec.from_frequencies(table)
    decoded = codec.decode(encoded)
    return(decoded)


"""
filename = 'rle_final_bad.txt'
file = open(filename, mode='r')
texte = file.read()
file.close()
"""

"""
huffmantxt = fromRLE_to_Huffman(texte)
print(huffmantxt)
print(decode_huffman(huffmantxt[0], huffmantxt[1]))
print(texte == decode_huffman(huffmantxt[0], huffmantxt[1]))
"""


"""
# Si on veut voir l'arbre de Huffman créé :

codec.print_code_table()

# Pour la décompression :

decoded = codec.decode(encoded)

print(decoded == texte)
"""
