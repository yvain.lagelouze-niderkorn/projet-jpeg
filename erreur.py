import numpy as np
from ast import Raise
from logging import raiseExceptions
import jpeg_main as jp
import decoupage_img_blocs as decoup

# We calculate the error of the pixture with the difference pixel by pixel of the matrix
# after compression/decompression and the matrix corresponding to the picture before compression


def Norme_matricielle(N):
    N = np.array(N)
    Norme = 0
    for i in range(np.shape(N)[0]):
        for j in range(np.shape(N)[1]):
            Norme += ((N[i][j][0] + N[i][j][1] + N[i][j][2]) / 3)**2
    return Norme

# Average of squared error (from the value of each pixel)


def Erreur_quadratique_moyenne(N, M):
    N = np.array(N)
    M = np.array(M)
    if np.shape(N) != np.shape(M):
        raise ValueError('Les deux matrices ne sont pas de la même taille')
    E = 0
    for i in range(np.shape(N)[0]):
        for j in range(np.shape(N)[1]):
            E += (N[i][j]-M[i][j])**2
    E = E/(len(N)*len(N[0]))
    return E

# Total error normalised (between 0 and 1)


def Erreur_totale(N, M):
    N = np.array(N)
    M = np.array(M)
    if np.shape(N) != np.shape(M):
        raise ValueError("The two matrix don't have the same size")
    E = Norme_matricielle(N-M)/Norme_matricielle(N)
    return(E)


print(len(jp.image), len(jp.decompress))

print(Erreur_totale(decoup.recadrer(jp.image), jp.decompress))
