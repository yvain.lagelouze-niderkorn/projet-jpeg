import re


def occurrences(string, i):
    ''' Returns the number of consecutive occurrences of the i-th character in string
    and the index of the last occurrence 
    '''
    n = 0
    ref = string[i]
    check = True
    for char in string[i:]:
        if not check:
            return {'letter': ref, 'occur': n, 'next_index': n+i}
        else:
            if char == ref:
                n += 1
            else:
                check = False
    return {'letter': ref, 'occur': n, 'next_index': n+i}


def occurrences2(string, i):
    ''' Returns the number of consecutive occurrences of the i-th character in string
    and the index of the last occurrence 
    '''
    n = 0
    ref = string[i]
    next = i
    for k in range(len(string)):
        char = string[k]
        if char == ref:
            n += 1
            next = k
    return {'letter': ref, 'occur': n, 'next_index': next}


input = '1111111111111111'


def rle(string):
    ''' Encode in the RLE format a given string
    The chosen separator is *
    '''
    output = ''
    length = len(string)
    index = 0
    while index < length:
        if str(string[index]) == '0':
            occur = occurrences(string, index)
            index = occur['next_index']
            oc = occur['occur']
            let = occur['letter']
            output += '*' + str(oc)+'*'+str(let) + ","
        else:
            output += str(string[index]) + ","
            index += 1
    return (output[:len(output)-1])


def decode_rle2(string):
    '''Decodes an RLE-encoded string
    The RLE separator must be *
    '''
    result = str()
    pattern = re.compile('\d*\*')
    i = 0
    while i < len(string):
        if string[i] == '*':
            corres_number = re.match(pattern, string[i+1:])
            number = corres_number.group()[:-1]
            span = corres_number.span()
            size = span[1]-span[0]
            result += int(number)*string[i+size+1]
            i += size+2
        else:
            result += string[i]
            i += 1
    return result


def str_to_int(string):  # converts a string into an int list
    if string[0] == "*":
        index = 1
        while string[index] != "*":  # The symbol * is at the position "index"
            index += 1
        nb = int(string[1:index])
        intLst = [0 for j in range(nb)]
    else:
        intLst = [int(string)]
    return(intLst)


'''Decodes an RLE-encoded string
    The RLE separator must be *
    '''

# Input : string encoding in RLE
# Output : list of integer


def decode_rle(string):
    result = []
    index = 0
    for k in range(len(string)):
        char = string[k]
        if char == ",":
            result += (str_to_int(string[index:k]))
            index = k+1  # New beginning for the next number
    result += (str_to_int(string[index:len(string)]))
    return result


"""
encoded_msg = rle(input)
print(encoded_msg)

decoded_msg = decode_rle(encoded_msg)
print(decoded_msg)
print(len(decoded_msg))
"""

# print(occurrences("143333394206428350", 2))
# print(rle("12331014014000000400040420000"))

"""
print(rle(["1", "10", "10502", "0", "0", "0", "10", "0"]))

print(str_to_int("*3*0"))

print(decode_rle("1,10,10502,*3*0,10,*1*0"))
"""

# print(decode_rle("-12,*1*0,-1,*61*0"))
